import toTsv from "../grabbers/fns/toTsv"

export default ChecksRuJson2Tsv

function ChecksRuJson2Tsv() {
  return <div>
      <label htmlFor="input">JSON</label>
      <textarea id="input"/>
      <button onClick={processing}>Process</button>
      <textarea id="output"/>
  </div>
}

function processing() {
  const data = toTsv(
      JSON.parse(
      (document.getElementById("input") as HTMLTextAreaElement)
      .value
    )
  )
  , el = document.getElementById("output") as HTMLTextAreaElement
  
  el.value = data

  // https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Interact_with_the_clipboard
  el.select()
  document.execCommand("copy")
}

