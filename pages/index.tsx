export default Index

function Index() {
  //TODO @ `next.config.js`
  const pages = ["fns"]
  return <ul>{
    pages.map(href => <li><a {...{href}}>{href}</a></li>)
  }</ul>
}