import type {tCheck} from "./checks.def"

export type tRecord = [
  agent: string,
  dateTime: string,
  total: number,
  name: string,
  price: number,
  quantity: number,
  sum: number
]

export default toTsv

function toTsv(checks: tCheck[]) {
  const csv: string[] = [
    row(
      "shop.id",
      "dateTime",
      "total",
      "product",
      "price",
      "quantity",
      "sum"              
    )
  ]

  for (
    const {"ticket": {"document": {"receipt": {
      dateTime,
      items,
      "totalSum": total,
      "userInn": agent
    }}}} of checks
  ) 
    for (const {name, price, quantity, sum} of items)
      csv.push(row(
        agent,
        new Date(dateTime).toLocaleString("en-GB").replace(",", ""),
        total / 100,
        name,
        price / 100,
        quantity,
        sum / 100         
      ))    

  return csv.join("\n")
}


function row(...args: Array<string|number>) {
  return args.join("\t")
}
