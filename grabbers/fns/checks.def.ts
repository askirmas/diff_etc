export type tCheck = {"ticket": {"document": {"receipt": {
  "dateTime": string
  "totalSum": number
  "userInn": string
  "items": Array<{
    "name": string
    "price": number
    "quantity": number
    "sum": number
  }>
}}}}
