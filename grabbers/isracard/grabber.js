async function grab(
  userGuid,
  cardsNumber,
  startYear,
  endYear = new Date().getFullYear(),
  isracardBase = "https://digital.isracard.co.il/services/ProxyRequestHandler.ashx?reqName=CardsTransactionsList&requiredDate=N&"
) {
  const transactions = new Array(cardsNumber)

  for (let y = endYear; y >= startYear; --y)
    for (let m = 12; m; --m)
      for (let c = cardsNumber; c--;) {
        const card = transactions[c] = transactions[c] ?? {}
        , year = card[y] = card[y] ?? {}

        year[m] = await(fetch(`${isracardBase}userGuid=${userGuid}&month=${m}&year=${y}&cardIdx=${c}`))
        .then(r => r.json())
      }
  
  return transactions
}
