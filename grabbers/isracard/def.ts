import schema from "./schema.json"

type Schema = typeof schema
type sTransaction = Schema["definitions"]["Transaction"]
// type sMetaSome = Schema["definitions"]["MetaSome"]

export type tSchema = Array<Record<string, Record<string, {
  /** `Index\d+` */
  "CardsTransactionsListBean": Record<string, {"CurrentCardTransactions": Array<Record<"txnIsrael"|"txnAbroad"|"txnInfo", tTransaction[]>>}>
}>>>
export type tTransaction = Record<
  keyof (/*sMetaSome["properties"] &*/ sTransaction["properties"]),
  string|null|number
>