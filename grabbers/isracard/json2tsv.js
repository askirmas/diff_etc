#!/usr/bin/env node 

const {keys: $keys} = Object
, omits = new Set(["", null, undefined])

if (require.main === module) {
  const headers = new Map()
  , tsv = [""]
  let headersRow = ""

  for (const filePath of process.argv.slice(2)) {
    const cardTsv = json2tsv(require(filePath, headers))
    headersRow = cardTsv[0]
    cardTsv[0] = ""
    tsv.push(cardTsv.join("\n"))
  }

  console.log(`${
    headersRow
  }\n${
    tsv.join("\n")
  }`)
}

/**
 * @param {import("./def").tSchema} source
 * @type {Map<string, number>} 
*/
function json2tsv(source, headers = new Map()) {
  const tsv = [""]

  for (let cardIndex = source.length; cardIndex--;) {
    const cardTransactions = source[cardIndex]

    for (const year in cardTransactions) {
      const yearTransactions = cardTransactions[year]

      for (const month in yearTransactions) {
        const {"CardsTransactionsListBean": _transactions} = yearTransactions[month]
        , indexes = $keys(_transactions).filter(key => key.startsWith('Index'))

        if (indexes.length !== 1)
          return [cardIndex, year, month, indexes]

        for (const transactionBunches of _transactions[indexes[0]].CurrentCardTransactions) {
          const {txnAbroad, txnInfo, txnIsrael} = transactionBunches
          , transactions = [txnAbroad, txnInfo, txnIsrael].filter(Boolean).flat()

          for (const transaction of transactions) {
            const row = new Array(headers.size).fill("")

            for (const key in transaction) {
              const value = transaction[key]
              if (key === "specificDate" && value !== null)
                console.log({"specificDate": value})
              if (omits.has(value))
                continue
              if (!headers.has(key))
                headers.set(key, headers.size)

              row[headers.get(key)] = value
            }

            tsv.push(row.join("\t"))
          }
        }
      }
    }
  }

  tsv[0] = [...headers.keys()].join("\t")
  return tsv
}