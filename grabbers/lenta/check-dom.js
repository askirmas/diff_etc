$q(".receipt-product__main-content")
.map(el => {
  const [title, count, sum] = $q(".receipt-product__title, .receipt-product__count, .receipt-product__final-price", el)
  .map(({"textContent": t}, i) => i ? parseFloat(t.replace(',', '.')) : t)
  return [
    title, sum/count, count, sum,
  ].join("\t")

})
.join("\n")

/**
 * @param {string} selector 
 * @param {HTMLElement} el 
 * @returns {HTMLElement[]}
 */
function $q(selector, el = document) {
  //@ts-ignore
  return [...el.querySelectorAll(selector)]
}